\begin{thebibliography}{128}
\providecommand{\natexlab}[1]{#1}
\providecommand{\url}[1]{\texttt{#1}}
\expandafter\ifx\csname urlstyle\endcsname\relax
  \providecommand{\doi}[1]{doi: #1}\else
  \providecommand{\doi}{doi: \begingroup \urlstyle{rm}\Url}\fi

\bibitem[Abliz(2011)]{abliz2011}
M.~Abliz.
\newblock {Internet Denial of Service Attacks and Defense Mechanisms}.
\newblock Technical report, Department of Computer Science, University of
  Pittsburgh, 2011.

\bibitem[AbuHmed et~al.(2007)AbuHmed, Mohaisen, and Nyang]{abuhmed2007}
T.~AbuHmed, A.~Mohaisen, and D.~Nyang.
\newblock {A Survey on Deep Packet Inspection for Intrusion Detection Systems}.
\newblock \emph{Journal of Korean Communications (Information and
  Communication)}, 24\penalty0 (11):\penalty0 25--36, 2007.

\bibitem[Adeyinka(2008)]{adeyinka2008}
O.~Adeyinka.
\newblock {Internet Attack Methods and Internet Security Technology}.
\newblock In \emph{Second Asia International Conference on Modeling \&
  Simulation, 2008. AICMS 08}, pages 77--82, Kuala Lumpur, Malaysia, 2008.
  IEEE.

\bibitem[Aho(1990)]{aho1990}
A.~Aho.
\newblock \emph{{Algorithms for Finding Patterns in Strings}}, chapter~5, pages
  255--300.
\newblock Handbook of Theoretical Computer Science, Volume A, Algorithms and
  Complexity. Elsevier, 1990.

\bibitem[Aho et~al.(1974)Aho, Hopcraft, and Ullman]{aho1974}
A.~Aho, J.~Hopcraft, and J.~Ullman.
\newblock \emph{{The Design and Analysis of Computer Algorithms}}.
\newblock 978-0-201-00029-0. Addison-Wesley, 1974.

\bibitem[Al-Shaer and Hamed(2003)]{shaer2003}
E.~Al-Shaer and H.~Hamed.
\newblock {Firewall Policy Advisor for Anomaly Discovery and Rule Editing}.
\newblock In G.~Goldszmidt and J.~Sch\"onw\"alder, editors, \emph{IFIP/IEEE
  Eighth International Symposium on Integrated Network Management, 2003}, pages
  17--30. IEEE, 2003.

\bibitem[Alshammari and Zincir-Heywood(2011)]{alshammar2011}
R.~Alshammari and A.~Zincir-Heywood.
\newblock {Can Encrypted Traffic Be Identified Without Port Numbers, IP
  Addresses and Payload Inspection?}
\newblock \emph{Computer Networks}, 55\penalty0 (6):\penalty0 1326--1350, 2011.

\bibitem[Anderson(1980)]{anderson1980}
J.~Anderson.
\newblock {Computer Security Threat Monitoring and Surveillance}.
\newblock Technical report, James P. Anderson Company, Fort Washington,
  Pennsylvania, 1980.

\bibitem[Anthony(2015)]{anthony2015}
S.~Anthony.
\newblock {GitHub Battles ``largest DDOS'' in Site's History, Targeted at
  Anti-censorship Tools}.
\newblock Online
  \url{http://arstechnica.com/security/2015/03/github-battles-largest-ddos-in-sites-history-targeted-at-anti-censorship-tools/}
  Date Accessed: 29 April 2016, 2015.

\bibitem[Apostolico and Crochemore(1991)]{apostolico1991}
A.~Apostolico and M.~Crochemore.
\newblock {Optimal Canonization of all Substrings of a String}.
\newblock \emph{Information and Computation}, 95\penalty0 (1):\penalty0 76--95,
  1991.

\bibitem[Apostolico and Giancarlo(1986)]{apostolico1986}
A.~Apostolico and R.~Giancarlo.
\newblock {The Boyer Moore Galil String Searching Strategies Revisited}.
\newblock \emph{SIAM Journal on Computing}, 15\penalty0 (1):\penalty0 98--105,
  1986.

\bibitem[Aschenbrenner(1986)]{aschenbrenner1986}
J.~Aschenbrenner.
\newblock {Open Systems Interconnection}.
\newblock \emph{IBM Systems Journal}, 25\penalty0 (3.4):\penalty0 369--379,
  1986.

\bibitem[Ashoor and Gore(2011)]{ashoor2011}
A.~Ashoor and S.~Gore.
\newblock {Importance of Intrusion Detection System (IDS)}.
\newblock \emph{International Journal of Scientific and Engineering Research},
  2\penalty0 (1):\penalty0 1--4, 2011.

\bibitem[Avolio(1999)]{avolio1999}
F.~Avolio.
\newblock {Firewalls and Internet Security, the Second Hundred (Internet)
  Years}.
\newblock \emph{The Internet Protocol Journal}, 2\penalty0 (4):\penalty0
  24--32, 1999.

\bibitem[Bachman(1894)]{bachman1894}
P.~Bachman.
\newblock \emph{{Die Analytische Zahlentheorie}}, volume~2.
\newblock Teubner, 1894.

\bibitem[Baeza-Yates and Gonnet(1992)]{baezayates1992}
R.~Baeza-Yates and G.~Gonnet.
\newblock {A New Approach to Text Searching}.
\newblock \emph{Communications of the ACM}, 35\penalty0 (10):\penalty0 74--82,
  1992.

\bibitem[Becchi et~al.(2009)Becchi, Wiseman, and Crowley]{becchi2009}
M.~Becchi, C.~Wiseman, and P.~Crowley.
\newblock {Evaluating Regular Expression Matching Engines on Network and
  General Purpose Processors}.
\newblock In P.~Onufryk, K.~Ramakrishnan, P.~Crowley, and J.~Wroclawski,
  editors, \emph{Proceedings of the 5th ACM/IEEE Symposium on Architectures for
  Networking and Communications Systems}, pages 30--39. ACM, 2009.

\bibitem[Bemer(1960)]{bemer1960}
R.~Bemer.
\newblock {A Proposal for Character Code Compatibility}.
\newblock \emph{Communications of the ACM}, 3\penalty0 (2):\penalty0 71--72,
  1960.

\bibitem[Bendrath and Mueller(2011)]{bendrath2011}
R.~Bendrath and M.~Mueller.
\newblock {The End of the Net as We Know It? Deep Packet Inspection and
  Internet Governance}.
\newblock \emph{New Media \& Society}, 13\penalty0 (7):\penalty0 1142--1160,
  2011.

\bibitem[Bonfiglio et~al.(2007)Bonfiglio, Mellia, Meo, Rossi, and
  Tofanelli]{bonfiglio2007}
D.~Bonfiglio, M.~Mellia, M.~Meo, D.~Rossi, and P.~Tofanelli.
\newblock {Revealing Skype Traffic: When Randomness Plays with You}.
\newblock \emph{ACM SIGCOMM Computer Communication Review}, 37\penalty0
  (4):\penalty0 37--48, 2007.

\bibitem[Borman et~al.(1999)Borman, Deering, and Hinden]{borman1999}
D.~Borman, S.~Deering, and R.~Hinden.
\newblock {Request for Comments: 2675 - IPv6 Jumbograms}.
\newblock Technical report, IETF, 1999.

\bibitem[Boyer and Moore(1977)]{boyer1977}
R.~Boyer and J.~Moore.
\newblock {A Fast String Searching Algorithm}.
\newblock \emph{Communications of the ACM}, 20\penalty0 (10):\penalty0
  762--772, 1977.

\bibitem[Bray et~al.(1998)Bray, Paoli, and Sperberg-McQueen]{bray1998}
T.~Bray, J.~Paoli, and C.~Sperberg-McQueen.
\newblock {Extensible Markup Language (XML) 1.0}.
\newblock Technical report, World Wide Web Consortium, 1998.

\bibitem[Breslauer(1992)]{breslauer1992}
D.~Breslauer.
\newblock \emph{{Efficient String Algorithmics}}.
\newblock PhD thesis, Computer Science Department, Columbia University, New
  York, New York, 1992.

\bibitem[Callado et~al.(2010)Callado, Kelner, Sadok, Kamienski, and
  Fernandes]{callado2010}
A.~Callado, J.~Kelner, D.~Sadok, C.~Kamienski, and S.~Fernandes.
\newblock {Better Network Traffic Identification through the Independent
  Combination of Techniques}.
\newblock \emph{Journal of Network and Computer Applications}, 33\penalty0
  (4):\penalty0 433--446, 2010.

\bibitem[Chandola et~al.(2009)Chandola, Banerjee, and Kumar]{chandola2009}
V.~Chandola, A.~Banerjee, and V.~Kumar.
\newblock { Anomaly detection: A survey}.
\newblock \emph{ACM Computing Surveys}, 2009.

\bibitem[Charras and Lecroq(2004)]{charras2004}
C.~Charras and T.~Lecroq.
\newblock \emph{{Handbook of Exact String-Matching Algorithms}}.
\newblock Institut d'{\'e}lectronique et d'informatique Gaspard-Monge, 2004.

\bibitem[Chatel(1996)]{chatel1996}
M.~Chatel.
\newblock {Request for Comments: 1919 - Classical versus Transparent IP
  Proxies}.
\newblock Technical report, IETF, 1996.

\bibitem[Chaudhary and Sardana(2011)]{chaudhary2011}
A.~Chaudhary and A.~Sardana.
\newblock {Software Based Implementation Methodologies for Deep Packet
  Inspection}.
\newblock In \emph{2011 International Conference on Information Science and
  Applications}, pages 1--10, Jeju Island, Republic of Korea, 2011. IEEE.

\bibitem[Cheswick(1990)]{cheswick1990}
B.~Cheswick.
\newblock {The Design of a Secure Internet Gateway}.
\newblock In \emph{USENIX Summer Conference Proceedings}, 1990.

\bibitem[Colussi(1991)]{colussi1991}
L.~Colussi.
\newblock {Correctness and Efficiency of Pattern Matching Algorithms}.
\newblock \emph{Information and Computation}, 95\penalty0 (2):\penalty0
  225--251, 1991.

\bibitem[Colussi(1994)]{colussi1994}
L.~Colussi.
\newblock {Fastest Pattern Matching in Strings}.
\newblock \emph{Journal of Algorithms}, 16\penalty0 (2):\penalty0 163--189,
  1994.

\bibitem[Corbridge et~al.(1991)Corbridge, Henig, and Slater]{corbridge1991}
B.~Corbridge, R.~Henig, and C.~Slater.
\newblock {Packet Filtering in an IP Router}.
\newblock In \emph{Proceedings of the Fifth USENIX Large Installation and
  System Administration Conference}, pages 227--232, 1991.

\bibitem[Crochemore and Lecroq(1996)]{crochemore1996}
M.~Crochemore and T.~Lecroq.
\newblock \emph{{Pattern Matching and Text Compression Algorithms}}, chapter~8,
  pages 162--202.
\newblock CRC Press Inc., Boca Raton, Florida, 1996.

\bibitem[Crochemore and Wojciech(2002)]{crochemore2002}
M.~Crochemore and R.~Wojciech.
\newblock \emph{{Jewels of Stringology: Text Algorithms}}.
\newblock World Scientific, 2002.

\bibitem[Crochemore et~al.(1994)Crochemore, Czumaj, Gasieniec, Jarominek,
  Lecroq, Plandowski, and Rytter]{crochemore1994}
M.~Crochemore, A.~Czumaj, L.~Gasieniec, S.~Jarominek, T.~Lecroq, W.~Plandowski,
  and W.~Rytter.
\newblock {Speeding up Two String-Matching Algorithms}.
\newblock \emph{Algorithmica}, 12\penalty0 (4-5):\penalty0 247--267, 1994.

\bibitem[Crockford(2006)]{crockford2006}
D.~Crockford.
\newblock {Request for Comments: 4627 - The application/json Media Type for
  JavaScript Object Notation (JSON)}.
\newblock Technical report, IETF, 2006.

\bibitem[Deering and Hinden(1998)]{deering1998}
S.~Deering and R.~Hinden.
\newblock {Request for Comments: 2460 - Internet Protocol, Version 6 (IPv6)
  Specification}.
\newblock Technical report, IETF, 1998.

\bibitem[Denning(1987)]{denning1987}
D.~Denning.
\newblock {An Intrusion-Detection Model}.
\newblock \emph{IEEE Transactions on Software Engineering}, \penalty0
  (2):\penalty0 222--232, 1987.

\bibitem[Denning(1989)]{denning1989}
P.~Denning.
\newblock {The Science of Computing: The Internet Worm}.
\newblock \emph{American Scientist}, 77\penalty0 (2):\penalty0 126--128, 1989.

\bibitem[Dharmapurikar et~al.(2003)Dharmapurikar, Krishnamurthy, Sproull, and
  Lockwood]{dharmapurikar2003}
S.~Dharmapurikar, P.~Krishnamurthy, T.~Sproull, and J.~Lockwood.
\newblock {Deep Packet Inspection using Parallel Bloom Filters}.
\newblock In D.~Azada, editor, \emph{2003 IEEE 11th Annual Symposium on
  High-Performance Interconnects}, pages 44--51, Stanford University, Stanford,
  California, 2003. IEEE.

\bibitem[Dougligeris and Mitrokotsa(2004)]{douligeris2004}
C.~Dougligeris and A.~Mitrokotsa.
\newblock {DDoS Attacks and Defense Mechanisms: Classification and
  State-of-the-Art}.
\newblock \emph{Computer Networks}, 44\penalty0 (5):\penalty0 643--666, 2004.

\bibitem[Dowd and McHenry(1998)]{dowd1998}
P.~W. Dowd and J.~T. McHenry.
\newblock {Network Security: It's Time to Take It Seriously}.
\newblock \emph{Computer}, 31\penalty0 (9):\penalty0 24--28, 1998.

\bibitem[Egevang and Francis(1994)]{egevang1994}
K.~Egevang and P.~Francis.
\newblock {Request for Comments: 1631 - The IP Network Address Translator
  (NAT)}.
\newblock Technical report, IETF, 1994.

\bibitem[Eisenberg et~al.(1989)Eisenberg, Gries, Hartmanis, Holcomb, and
  Lynn]{eisenberg1989}
T.~Eisenberg, D.~Gries, J.~Hartmanis, D.~Holcomb, and M.~S. Lynn.
\newblock {The Cornell Commission: on Morris and the Worm}.
\newblock \emph{Communications of the ACM}, 32\penalty0 (6):\penalty0 706--709,
  1989.

\bibitem[Eisenmann et~al.(2009)Eisenmann, Parker, and van
  Alstyne]{eisenmann2009}
T.~Eisenmann, G.~Parker, and M.~van Alstyne.
\newblock {Opening Platforms: How, When and Why?}
\newblock In A.~Gawer, editor, \emph{Platforms, Markets and Innovation}. Edward
  Elgar Publishing, 2009.

\bibitem[Fan et~al.(2009)Fan, Yao, and Ma]{fan2009}
H.~Fan, N.~Yao, and H.~Ma.
\newblock {Fast Variants of the Backward-Oracle-Marching Algorithm}.
\newblock In \emph{Fourth International Conference on Internet Computing for
  Science and Engineering}, pages 56--59, 2009.

\bibitem[Faro and Lecroq(2009)]{faro2009}
S.~Faro and T.~Lecroq.
\newblock {Efficient Variants of the Backward-Oracle-Matching Algorithm}.
\newblock In 20, editor, \emph{International Journal of Foundations of Computer
  Science}, volume~6, pages 967--984. World Scientific, 2009.

\bibitem[Faro and Lecroq(2011)]{faro2011}
S.~Faro and T.~Lecroq.
\newblock {SMART: String Matching Research Tool}.
\newblock Online \url{http://www.dmi.unict.it/~faro/smart/} Date Accessed: 10
  May 2016, 2011.

\bibitem[Faro and Lecroq(2013)]{faro2013}
S.~Faro and T.~Lecroq.
\newblock {The Exact Online String Matching Problem: A Review of the Most
  Recent Results}.
\newblock \emph{ACM Computing Surveys}, 45\penalty0 (2):\penalty0 13, 2013.

\bibitem[Feng and Takaoka(1987)]{feng1987}
Z.~R. Feng and T.~Takaoka.
\newblock {On Improving the Average Case of the Boyer-Moore String Matching
  Algorithm}.
\newblock \emph{Journal of Information Processing}, 10\penalty0 (3):\penalty0
  173--177, 1987.

\bibitem[Ferguson and Senie(2000)]{ferguson2000}
P.~Ferguson and D.~Senie.
\newblock {Request for Comments: 2827 - Network Ingress Filtering: Defeating
  Denial of Service Attacks which employ IP Source Address Spoofing}.
\newblock Technical report, IETF, 2000.

\bibitem[Fowler et~al.(1990)Fowler, Fowler, and Allen]{oxford2016}
H.~Fowler, F.~Fowler, and R.~Allen.
\newblock \emph{{The Concise Oxford Dictionary: firewall, n.}}
\newblock ISBN: 0-19-861200-1. Clarendon Press - Oxford, 1990.

\bibitem[Galil and Giancarlo(1992)]{galil1992}
Z.~Galil and R.~Giancarlo.
\newblock {On the Exact Complexity of String Matching: Upper Bounds}.
\newblock \emph{SIAM Journal on Computing}, 21\penalty0 (3):\penalty0 407--437,
  1992.

\bibitem[Gantz et~al.(2014)Gantz, Florean, Lee, Lim, Sikdar, Lakshmi, Madhavan,
  and Nagappan]{gantz2014}
J.~Gantz, A.~Florean, R.~Lee, V.~Lim, B.~Sikdar, S.~Lakshmi, L.~Madhavan, and
  M.~Nagappan.
\newblock {The Link between Pirated Software and Cybersecurity Breaches}.
\newblock Technical report, National University of Singapore and IDC, 2014.

\bibitem[Garcia(2008)]{garcia2008}
L.~Garcia.
\newblock {Programming with Libpcap - Sniffing the Network From Our Own
  Applications}.
\newblock In \emph{Hakin9}, volume February 2008, pages 38--46. HAKIN9 MEDIA
  SP, 2008.

\bibitem[Gardner(1989)]{gardner1989}
P.~Gardner.
\newblock {The Internet Worm: What Was Said and When}.
\newblock \emph{Computers and Security}, 8\penalty0 (4):\penalty0 305--316,
  1989.

\bibitem[Ghosh et~al.(1998)Ghosh, Wanken, and Charron]{ghosh1998}
A.~Ghosh, J.~Wanken, and F.~Charron.
\newblock {Detecting Anomalous and Unknown Intrusions Against Programs}.
\newblock In \emph{Proceedings. 14th Annual Computer Security Applications
  Conference, 1998}, pages 259--267. IEEE, 1998.

\bibitem[Grondman(2006)]{grondman2006}
I.~Grondman.
\newblock {Identifying Short-term Periodicities in Internet Traffic}.
\newblock Master's thesis, University of Twente, 2006.

\bibitem[Gupta(2013)]{gupta2013}
V.~Gupta.
\newblock {File Detection in Network Traffic Using Approximate Matching}.
\newblock Master's thesis, Norwegian University of Science and Technology,
  2013.

\bibitem[Haertel(2010)]{haertel2010}
M.~Haertel.
\newblock {Why GNU grep is Fast}.
\newblock Online
  \url{https://lists.freebsd.org/pipermail/freebsd-current/2010-August/019310.html}
  Date Accessed: 9 May 2016, 2010.

\bibitem[Hancart(1993)]{hancart1993}
C.~Hancart.
\newblock \emph{{Analyse Exacte et en Moyenne D'algorithmes de Recherche D'un
  Motif dans un Texte}}.
\newblock PhD thesis, Universit{\'e} Paris Diderot, 1993.

\bibitem[Handley et~al.(2001)Handley, Paxson, and Kreibich]{handley2001}
M.~Handley, V.~Paxson, and C.~Kreibich.
\newblock {Network Intrusion Detection: Evasion, Traffic Normalization, and
  End-to-End Protocol Semantics}.
\newblock In \emph{USENIX Security Symposium}, pages 115--131, 2001.

\bibitem[Hauben and Hauben(2006)]{hauben2006}
M.~Hauben and R.~Hauben.
\newblock {Behind the Net: The Untold History of the ARPANET and Computer
  Science}.
\newblock \emph{Netizens: On the History and Impact of Usenet and the
  Internet}, 2006.

\bibitem[Hibberd(2012)]{hibberd2012}
M.~Hibberd.
\newblock {Encryption: Will It Be the Death of DPI?}
\newblock Online
  \url{http://telecoms.com/39718/encryption-will-it-be-the-death-of-dpi/} Date
  Accessed: 03 May 2016, 2012.

\bibitem[Hoffman(2013)]{hoffman2013}
S.~Hoffman.
\newblock {DDoS: A Brief History}.
\newblock Online \url{https://blog.fortinet.com/post/ddos-a-brief-history} Date
  Accessed: 9 May 2016, 2013.

\bibitem[Horspool(1980)]{horspool1980}
R.~N Horspool.
\newblock {Practical Fast Searching Strings}.
\newblock \emph{Software: Practice and Experience}, 10\penalty0 (6):\penalty0
  501--506, 1980.

\bibitem[Ihaka and Gentleman(1996)]{ihaka1996}
R.~Ihaka and R.~Gentleman.
\newblock {R: A Language for Data Analysis and Graphics}.
\newblock \emph{Journal of Computational and Graphical Statistics}, 5\penalty0
  (3):\penalty0 299--314, 1996.

\bibitem[Ingham and Forrest(2002)]{ingham2002}
K.~Ingham and S.~Forrest.
\newblock A history and survey of network firewalls.
\newblock Technical report, University of New Mexico, 2002.

\bibitem[Jenkov(2014)]{jenkov2014}
J.~Jenkov.
\newblock {Java Concurrency / Multithreading Tutorial}.
\newblock Online \url{http://tutorials.jenkov.com/java-concurrency/index.html}
  Date Accessed: 9 May 2016, 2014.

\bibitem[Jiang and Prassana(2009)]{jiang2009}
W.~Jiang and V.~Prassana.
\newblock {Large-Scale Wire-Speed Packet Classification on FPGAs}.
\newblock In \emph{Proceedings of the ACM/SIGDA international symposium on
  Field Programmable Gate Arrays}, pages 219--228. ACM, 2009.

\bibitem[Karp and Rabin(1987)]{karp1987}
R.~M. Karp and M.~O. Rabin.
\newblock {Efficient Randomized Pattern-Matching Algorithms}.
\newblock \emph{IBM Journal of Research and Development}, 31\penalty0
  (2):\penalty0 249--260, 1987.

\bibitem[Kemmerer and Vigna(2002)]{kemmerer2002}
R.~Kemmerer and G.~Vigna.
\newblock {Intrusion Detection: A Brief History and Overview}.
\newblock \emph{Computer}, \penalty0 (4):\penalty0 27--30, 2002.

\bibitem[Knuth et~al.(1977)Knuth, Morris, and Pratt]{knuth1977}
D.~E. Knuth, J.~H. Morris, and V.~R. Pratt.
\newblock {Fast Pattern Matching in Strings}.
\newblock \emph{SIAM Journal on Computing}, 6\penalty0 (2):\penalty0 232--350,
  1977.

\bibitem[Koblas and Koblas(1992)]{koblas1992}
D.~Koblas and M.~Koblas.
\newblock {SOCKS}.
\newblock In \emph{In UNIX Security Sympsium III Proceedings}, 1992.

\bibitem[K\"ulekci(2009)]{kulekci2009}
M.~K\"ulekci.
\newblock {Filter Based Fast Matching of Long Patterns by Using SIMD
  Instructions }.
\newblock In J.~Holub and \v{Z}\v{d}\'{a}rek, editors, \emph{In Proceedings of
  the Prague Stringology Conference}, pages 118--128, 2009.

\bibitem[Kumar et~al.(2006)Kumar, Turner, and Williams]{kumar2006}
S.~Kumar, J.~Turner, and J.~Williams.
\newblock {Advanced Algorithms for Fast and Scalable Deep Packet Inspection}.
\newblock In \emph{Proceedings of the 2006 ACM/IEEE Symposium on Architecture
  for Networking and Communications Systems}, pages 81--92. ACM, 2006.

\bibitem[Landau(1909)]{landau1909}
E.~Landau.
\newblock \emph{{Handbuch der Lehre von der Verteilung der Primzahlen}},
  volume~1.
\newblock Teubner, 1909.

\bibitem[Law et~al.(2012)Law, Diab, Healy, Carlson, Maguire, Anslow, and
  Hajduczenia]{law2012}
D.~Law, W.~Diab, A.~Healy, S.~Carlson, V.~Maguire, O.~Anslow, and
  M.~Hajduczenia.
\newblock {IEEE Standard for Ethernet}.
\newblock Technical report, IEEE Standards Association, 2012.

\bibitem[Lecroq(1995)]{lecroq1995}
T.~Lecroq.
\newblock {Experimental Results on String Matching Algorithms}.
\newblock \emph{Software: Practice and Experience}, 25\penalty0 (7):\penalty0
  727--765, 1995.

\bibitem[Lecroq(2007)]{lecroq2007}
T.~Lecroq.
\newblock Fast exact string matching algorithms.
\newblock \emph{Information Processing Letters}, 102\penalty0 (6):\penalty0
  229--235, 2007.

\bibitem[Leiner et~al.(2009)Leiner, Cerf, Clark, Kah, Kleinrock, Lynch, Postel,
  Roberts, and Wolff]{leiner2009}
B.~Leiner, V.~Cerf, D.~Clark, R.~Kah, L~Kleinrock, D.~Lynch, J.~Postel,
  L.~Roberts, and S.~Wolff.
\newblock {A Brief History of the Internet}.
\newblock \emph{SIGCOMM Computer Communications Review}, 39\penalty0
  (5):\penalty0 22--31, 2009.

\bibitem[Lilly(2009)]{lilly2009}
P.~Lilly.
\newblock {A Brief History of CPUs: 31 Awesome Years of X86}.
\newblock Online
  \url{http://www.pcgamer.com/a-brief-history-of-cpus-31-awesome-years-of-x86/}
  Date Accessed: 10 May 2016, 2009.

\bibitem[Metcalf(2014)]{metcalf2014}
J.~Metcalf.
\newblock {Creeper \& Reaper}.
\newblock Online \url{http://corewar.co.uk/creeper.htm} Date Accessed: 4 May
  2016, 2014.

\bibitem[Mogul(1989)]{mogul1989}
J.~Mogul.
\newblock {Simple and Flexible Datagram Access Controls for Unix-based
  Gateways}.
\newblock Technical report, Western Research Laboratory, 1989.

\bibitem[Moore and Papagiannaki(2005)]{moore2005}
A.~Moore and K.~Papagiannaki.
\newblock {Toward the Accurate Identification of Network Applications}.
\newblock In C.~Dovrolis, editor, \emph{Passive and Active Network
  Measurement}. Springer, 2005.

\bibitem[Moore(1965)]{moore1965}
G.~Moore.
\newblock {Cramming More Components onto Integrated Circuits}.
\newblock \emph{Proceedings of the IEEE}, 3\penalty0 (20):\penalty0 33--35,
  1965.

\bibitem[Morris and Pratt(1970)]{morris1970}
J.~H. Morris and V.~R. Pratt.
\newblock {A Linear Pattern-Matching Algorithm}.
\newblock Technical report, University of California, Berkeley, 1970.

\bibitem[Mueller and Asghari(2012)]{mueller2012}
M.~Mueller and H.~Asghari.
\newblock {Deep Packet Inspection and Bandwidth Management: Battles over
  BitTorrent in Canada and the United States}.
\newblock \emph{Telecommunications Policy}, 36\penalty0 (6):\penalty0 462--475,
  2012.

\bibitem[Necker et~al.(2002)Necker, Contis, and Schimmel]{necker2002}
M.~Necker, D.~Contis, and D.~Schimmel.
\newblock {TCP-Stream Reassembly and State Tracking in Hardware}.
\newblock In \emph{10th Annual IEEE Symposium on Field-Programmable Custom
  Computing Machines, 2002}, pages 286--287. IEEE, 2002.

\bibitem[Needham(1993)]{needham1993}
R.~Needham.
\newblock {Denial of Service}.
\newblock In \emph{CCS '93 Proceedings of the 1st ACM Conference on Computer
  and Communications Security}, pages 151--153. ACM, 1993.

\bibitem[Neilsen(1998)]{neilsen1998}
J.~Neilsen.
\newblock {Nielsen's Law of Internet Bandwidth}.
\newblock Online \url{https://www.nngroup.com/articles/law-of-bandwidth/} Date
  Accessed: 07 April 2016, 1998.

\bibitem[Networks(2011)]{arbor2011}
Arbor Networks.
\newblock {Arbor E100}.
\newblock Online
  \url{http://www.lextel.com/wp-content/uploads/E100Datasheet.pdf} Date
  Accessed: 14 February 2016, 2011.

\bibitem[Networks(2016)]{paloalto2016}
Palo~Alto Networks.
\newblock {Education}.
\newblock Online \url{https://www.paloaltonetworks.com/services/education} Date
  Accessed: 1 May 2016, 2016.

\bibitem[Parsons(2009)]{parsons2009}
C.~Parsons.
\newblock {Deep Packet Inspection and Law Enforcement}.
\newblock Online
  \url{https://www.christopher-parsons.com/deep-packet-inspection-and-law-enforcement/}
  Date Accessed: 3 May 2016, 2009.

\bibitem[Parsons(2012)]{parsons2012}
C.~Parsons.
\newblock \emph{{Deep Packet Inspection and Its Predecessors}}.
\newblock PhD thesis, Department of Political Science, University of Victoria,
  2012.

\bibitem[Perrin(2008)]{perrin2008}
C.~Perrin.
\newblock {The CIA Triad}.
\newblock Online
  \url{http://www.techrepublic.com/blog/it-security/the-cia-triad/} Date
  Accessed: 2 May 2016, 2008.

\bibitem[Pike and Thompson(1993)]{pike1993}
R.~Pike and K.~Thompson.
\newblock {Hello World}.
\newblock In \emph{Proceedings of the Winter 1993 USENIX Conference, Berkeley,
  CA, USENIX Association}, pages 43--50. USENIX, 1993.

\bibitem[Postel(1981)]{postel1981}
J.~Postel.
\newblock {Request for Comments: 791 - Internet Protocol}.
\newblock Technical report, IETF, 1981.

\bibitem[Raita(1991)]{raita1991}
T.~Raita.
\newblock {Tuning the Boyer-Moore-Horspool String Searching Algorithm}.
\newblock \emph{Software: Practice and Experience}, 22\penalty0 (10):\penalty0
  879--884, 1991.

\bibitem[Ramirez(1999)]{ramirez1999}
G.~Ramirez.
\newblock {\texttt{randpkt}}.
\newblock Online
  \url{https://github.com/wireshark/wireshark/blob/master/randpkt.c} Date
  Accessed: 10 May 2016, 1999.

\bibitem[Ranum(1992)]{ranum1992}
M.~Ranum.
\newblock {A Network Firewall}.
\newblock In \emph{Proceedings of the World Conference on System Administration
  and Security}, 1992.

\bibitem[Rouse(2011)]{rouse2011}
M.~Rouse.
\newblock {Content Filtering (Information Filtering)}.
\newblock Online
  \url{http://searchsecurity.techtarget.com/definition/content-filtering} Date
  Accessed: 11 June 2016, 2011.

\bibitem[Schuff and Pai(2007)]{schuff2007}
D.~Schuff and V.~Pai.
\newblock {Design Alternatives for a High-Performance Self-Securing Ethernet
  Network Interface}.
\newblock In \emph{Parallel and Distributed Processing Symposium}, pages 1--10.
  IEEE, 2007.

\bibitem[Shah(2001)]{shah2001}
N.~Shah.
\newblock {Understanding Network Processors}.
\newblock Master's thesis, University of California, Berkeley, 2001.

\bibitem[Shankar and Paxon(2003)]{shankar2003}
U.~Shankar and V.~Paxon.
\newblock {Active Mapping: Resisting NIDS Evasion Without Altering Traffic}.
\newblock In \emph{2003 Symposium on Security and Privacy, 2003.}, pages
  44--61. IEEE, 2003.

\bibitem[Simon(1994)]{simon1994}
I.~Simon.
\newblock {String Matching Algorithms and Automata}.
\newblock In \emph{Proceedings of the Colloquium in Honor of Arto Salomaa on
  Results and Trends in Theoretical Computer Science}. Springer, 1994.

\bibitem[Smith(1991)]{smith1991}
D.~Smith, P.
\newblock {Experiments with a Very Fast Substring Search Algorithm}.
\newblock \emph{Software: Practice and Experience}, 21\penalty0 (10):\penalty0
  1065--1074, 1991.

\bibitem[Smith(1994)]{smith1994}
D.~Smith, P.
\newblock {On Tuning the Boyer-Moore-Horspool String Search Algorithms}.
\newblock \emph{Software: Practice and Experience}, 1994.

\bibitem[Sourdis(2007)]{sourdis2007}
I.~Sourdis.
\newblock \emph{{Designs and Algorithms for Packet and Content Inspection}}.
\newblock PhD thesis, Delft University of Technology, 2007.

\bibitem[Spafford(1989{\natexlab{a}})]{spafford1989a}
E.~Spafford.
\newblock {Crisis and Aftermath}.
\newblock \emph{Communications of the ACM}, 32\penalty0 (6):\penalty0 678--687,
  1989{\natexlab{a}}.

\bibitem[Spafford(1989{\natexlab{b}})]{spafford1989b}
E.~Spafford.
\newblock {The Internet Worm Program: An Analysis}.
\newblock \emph{Communications of the ACM}, 19\penalty0 (1):\penalty0 17--57,
  1989{\natexlab{b}}.

\bibitem[Srikantha et~al.(2010)Srikantha, Bopardikar, Kaipa, Venkataraman, Lee,
  Ahn, and Narayanan]{srikantha2010}
A.~Srikantha, A.~Bopardikar, K.~Kaipa, P.~Venkataraman, K.~Lee, T.~Ahn, and
  R.~Narayanan.
\newblock {A Fast Algorithm for Exact Sequence Search in Biological Sequences
  Using Polyphase Decomposition }.
\newblock \emph{Bioinformatics}, 26\penalty0 (18):\penalty0 i414--i419, 2010.

\bibitem[Stephen(1994)]{stephen1994}
G.~Stephen.
\newblock \emph{{String Search Algorithms}}, volume~3 of \emph{Lecture Notes
  Series on Computing}.
\newblock World Scientific, 1994.

\bibitem[Stoll(1989)]{stoll1989}
C.~Stoll.
\newblock \emph{{The Cuckoo's Egg: Tracking a Spy Through the Maze of Computer
  Espionage}}.
\newblock Doubleday, 1989.

\bibitem[Sunday(1990)]{sunday1990}
D.~M. Sunday.
\newblock {A Very Fast Substring Search Algorithm}.
\newblock \emph{Communications of the ACM}, 33\penalty0 (8):\penalty0 132--142,
  1990.

\bibitem[Thompson(1968)]{thompson1968}
K.~Thompson.
\newblock {Programming Techniques: Regular Expression Search Algorithm}.
\newblock \emph{Communications of the ACM}, 11\penalty0 (6):\penalty0 419--422,
  1968.

\bibitem[Thompson and Ritchie(1975)]{thompson1975}
K.~Thompson and D.~Ritchie.
\newblock {UNIX Proframmer's Manual}.
\newblock Technical report, Bell Telephone Laboratories, 1975.

\bibitem[Thomson et~al.(2003)Thomson, Huitema, Ksinant, and
  Souissi]{thomson2003}
S.~Thomson, C.~Huitema, V.~Ksinant, and M.~Souissi.
\newblock {Request for Comments: 3596 - DNS Extensions to Support IP Version
  6}.
\newblock Technical report, IETF, 2003.

\bibitem[/u/quink(2009)]{quink2009}
/u/quink.
\newblock {Here's a new scenario I just created illustrating what happens if
  net neutrality disappears. [PIC]}.
\newblock Online
  \url{https://www.reddit.com/comments/9yj1f/heres_a_new_scenario_i_just_created_illustrating},
  2009.

\bibitem[van Splunder(2015)]{splunder2015}
J.~van Splunder.
\newblock {Periodicity Detection in Network Traffic}.
\newblock Master's thesis, Mathematisch Instituut, Universiteit Leiden, 2015.

\bibitem[Ward(2001)]{ward2001}
M.~Ward.
\newblock {H@ppy Birthday to You}.
\newblock Online
  \url{http://news.bbc.co.uk/2/hi/in_depth/sci_tech/2000/dot_life/1586229.stm}
  Date Accessed: 6 May 2016, 2001.

\bibitem[Wickham(2011)]{wickham2011}
H.~Wickham.
\newblock {ggplot2}.
\newblock \emph{Wiley Interdisciplinary Reviews: Computational Statistics},
  3\penalty0 (2):\penalty0 180--185, 2011.

\bibitem[Wu and Manber(1992)]{wu1992}
S.~Wu and U.~Manber.
\newblock {Fast Text Searching: Allowing Errors}.
\newblock \emph{Communications of the ACM}, 35\penalty0 (10):\penalty0 83--91,
  1992.

\bibitem[Wuermeling(1989)]{wuermeling1989}
U.~Wuermeling.
\newblock {New Dimensions of Computer-Crime - Hacking for the KGB - A Report}.
\newblock \emph{Computer Law \& Security Review}, 5\penalty0 (4):\penalty0
  20--21, 1989.

\bibitem[Yang et~al.(2010)Yang, Liao, Luo, Wang, and Yeh]{yang2010}
C.~Yang, M.~Liao, M.~Luo, S.~Wang, and C.~Yeh.
\newblock {A Network Management System Based on DPI}.
\newblock In \emph{2010 13th International Conference on Network-Based
  Information Systems (NBiS)}, pages 385--388. IEEE, 2010.

\bibitem[Yu et~al.(2006)Yu, Chen, Diao, Lakshman, and Katz]{yu2006}
F.~Yu, Z.~Chen, Y.~Diao, T.~Lakshman, and R.~Katz.
\newblock {Fast and Memory-efficient Regular Expression Matching for Deep
  Packet Inspection}.
\newblock In \emph{Proceedings of the 2006 ACM/IEEE Symposium on Architecture
  for Networking and Communications Systems}, pages 93--102. ACM, 2006.

\bibitem[Zwicky et~al.(2000)Zwicky, Cooper, and Chapman]{zwicky2000}
E.~Zwicky, S.~Cooper, and D.~Chapman.
\newblock \emph{{Building Internet Firewalls}}.
\newblock O'Reilly Media, Inc., 2000.

\end{thebibliography}
