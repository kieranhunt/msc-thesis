\contentsline {part}{I\hspace {1em}Introduction}{1}{part.1}
\contentsline {chapter}{\numberline {1}Introduction}{2}{chapter.1}
\contentsline {section}{\numberline {1.1}Problem Statement}{3}{section.1.1}
\contentsline {section}{\numberline {1.2}Research Outline}{5}{section.1.2}
\contentsline {section}{\numberline {1.3}Research Method}{5}{section.1.3}
\contentsline {section}{\numberline {1.4}Document Conventions}{6}{section.1.4}
\contentsline {section}{\numberline {1.5}Document Structure}{6}{section.1.5}
\contentsline {chapter}{\numberline {2}Literature Review}{8}{chapter.2}
\contentsline {section}{\numberline {2.1}General Network Security}{8}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Network Security Tenets}{10}{subsection.2.1.1}
\contentsline {subsection}{\numberline {2.1.2}Common Network Security Threats}{11}{subsection.2.1.2}
\contentsline {subsection}{\numberline {2.1.3}Network Threat Mitigation Techniques}{12}{subsection.2.1.3}
\contentsline {subsection}{\numberline {2.1.4}Summary}{13}{subsection.2.1.4}
\contentsline {section}{\numberline {2.2}Firewalls}{14}{section.2.2}
\contentsline {subsection}{\numberline {2.2.1}Layer 7 - Application Layer}{16}{subsection.2.2.1}
\contentsline {subsection}{\numberline {2.2.2}Layer 4 - Transport Layer}{17}{subsection.2.2.2}
\contentsline {subsection}{\numberline {2.2.3}Layer 3 - Network Layer}{18}{subsection.2.2.3}
\contentsline {subsection}{\numberline {2.2.4}Network Address Translation}{19}{subsection.2.2.4}
\contentsline {subsection}{\numberline {2.2.5}Other Firewalling Techniques}{19}{subsection.2.2.5}
\contentsline {subsection}{\numberline {2.2.6}Denial of Service}{20}{subsection.2.2.6}
\contentsline {subsection}{\numberline {2.2.7}Shortfalls}{21}{subsection.2.2.7}
\contentsline {subsection}{\numberline {2.2.8}Summary}{21}{subsection.2.2.8}
\contentsline {section}{\numberline {2.3}Intrusion Detection Systems}{21}{section.2.3}
\contentsline {subsection}{\numberline {2.3.1}IDS Rules}{25}{subsection.2.3.1}
\contentsline {subsection}{\numberline {2.3.2}Effectiveness}{26}{subsection.2.3.2}
\contentsline {subsection}{\numberline {2.3.3}Performance}{27}{subsection.2.3.3}
\contentsline {subsubsection}{Implementation}{28}{section*.10}
\contentsline {section}{\numberline {2.4}Packet Inspection}{30}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Shallow Packet Inspection}{31}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Medium Packet Inspection}{31}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}Deep Packet Inspection}{33}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Encrypted Traffic}{35}{subsection.2.4.4}
\contentsline {subsection}{\numberline {2.4.5}Why Perform DPI?}{36}{subsection.2.4.5}
\contentsline {section}{\numberline {2.5}Summary}{38}{section.2.5}
\contentsline {chapter}{\numberline {3}Algorithms}{40}{chapter.3}
\contentsline {section}{\numberline {3.1}Stringology Primer}{41}{section.3.1}
\contentsline {section}{\numberline {3.2}Na{\"i}ve}{44}{section.3.2}
\contentsline {section}{\numberline {3.3}Morris-Pratt}{45}{section.3.3}
\contentsline {section}{\numberline {3.4}Knuth-Morris-Pratt}{45}{section.3.4}
\contentsline {section}{\numberline {3.5}Boyer-Moore}{45}{section.3.5}
\contentsline {section}{\numberline {3.6}Horspool}{46}{section.3.6}
\contentsline {section}{\numberline {3.7}Rabin-Karp}{46}{section.3.7}
\contentsline {section}{\numberline {3.8}Zhu-Takaoka}{47}{section.3.8}
\contentsline {section}{\numberline {3.9}Quick Search}{47}{section.3.9}
\contentsline {section}{\numberline {3.10}Smith}{47}{section.3.10}
\contentsline {section}{\numberline {3.11}Apostolico-Crochemore}{48}{section.3.11}
\contentsline {section}{\numberline {3.12}Colussi}{48}{section.3.12}
\contentsline {section}{\numberline {3.13}Raita}{48}{section.3.13}
\contentsline {section}{\numberline {3.14}Galil-Gaincarlo}{49}{section.3.14}
\contentsline {section}{\numberline {3.15}Bitap}{49}{section.3.15}
\contentsline {section}{\numberline {3.16}Simon}{49}{section.3.16}
\contentsline {section}{\numberline {3.17}Not So Naive}{50}{section.3.17}
\contentsline {section}{\numberline {3.18}Turbo Boyer-Moore}{50}{section.3.18}
\contentsline {section}{\numberline {3.19}Reverse Colussi}{50}{section.3.19}
\contentsline {section}{\numberline {3.20}Summary}{51}{section.3.20}
\contentsline {chapter}{\numberline {4}Datasets}{53}{chapter.4}
\contentsline {section}{\numberline {4.1}Dataset A}{53}{section.4.1}
\contentsline {section}{\numberline {4.2}Dataset B}{54}{section.4.2}
\contentsline {section}{\numberline {4.3}Dataset C}{55}{section.4.3}
\contentsline {section}{\numberline {4.4}Dataset D}{56}{section.4.4}
\contentsline {section}{\numberline {4.5}Dataset E}{57}{section.4.5}
\contentsline {section}{\numberline {4.6}Dataset F}{58}{section.4.6}
\contentsline {section}{\numberline {4.7}Summary}{59}{section.4.7}
\contentsline {part}{II\hspace {1em}Packet Inspection Framework}{60}{part.2}
\contentsline {chapter}{\numberline {5}Design}{61}{chapter.5}
\contentsline {section}{\numberline {5.1}Introduction}{61}{section.5.1}
\contentsline {section}{\numberline {5.2}Overall Design}{62}{section.5.2}
\contentsline {section}{\numberline {5.3}Input}{62}{section.5.3}
\contentsline {section}{\numberline {5.4}Processing}{63}{section.5.4}
\contentsline {section}{\numberline {5.5}Statistics Generation}{64}{section.5.5}
\contentsline {section}{\numberline {5.6}Statistical Output}{66}{section.5.6}
\contentsline {section}{\numberline {5.7}Raw Output}{67}{section.5.7}
\contentsline {section}{\numberline {5.8}Summary}{69}{section.5.8}
\contentsline {chapter}{\numberline {6}Implementation}{70}{chapter.6}
\contentsline {section}{\numberline {6.1}Example Test}{71}{section.6.1}
\contentsline {subsection}{\numberline {6.1.1}Program Startup}{71}{subsection.6.1.1}
\contentsline {subsection}{\numberline {6.1.2}Testing}{73}{subsection.6.1.2}
\contentsline {subsection}{\numberline {6.1.3}Statistics Generation}{74}{subsection.6.1.3}
\contentsline {subsection}{\numberline {6.1.4}Output}{75}{subsection.6.1.4}
\contentsline {subsection}{\numberline {6.1.5}Test Configuration}{77}{subsection.6.1.5}
\contentsline {subsection}{\numberline {6.1.6}Statistics Output}{79}{subsection.6.1.6}
\contentsline {subsection}{\numberline {6.1.7}Raw Results Output}{81}{subsection.6.1.7}
\contentsline {section}{\numberline {6.2}Summary}{83}{section.6.2}
\contentsline {part}{III\hspace {1em}Testing and Analysis}{84}{part.3}
\contentsline {chapter}{\numberline {7}Initial Algorithm Comparison}{85}{chapter.7}
\contentsline {section}{\numberline {7.1}Rules}{86}{section.7.1}
\contentsline {section}{\numberline {7.2}Test Hardware}{87}{section.7.2}
\contentsline {section}{\numberline {7.3}Algorithm Performance?}{87}{section.7.3}
\contentsline {subsection}{\numberline {7.3.1}\hyperref [section-dataseta]{\textit {Dataset \uppercase {a}}}\xspace }{88}{subsection.7.3.1}
\contentsline {subsection}{\numberline {7.3.2}\hyperref [section-datasetb]{\textit {Dataset \uppercase {b}}}\xspace }{90}{subsection.7.3.2}
\contentsline {section}{\numberline {7.4}Which algorithms vary the most?}{92}{section.7.4}
\contentsline {section}{\numberline {7.5}Length Impact on Performance}{95}{section.7.5}
\contentsline {subsection}{\numberline {7.5.1}Horspool}{98}{subsection.7.5.1}
\contentsline {subsection}{\numberline {7.5.2}Quick Search}{100}{subsection.7.5.2}
\contentsline {subsection}{\numberline {7.5.3}Not So Na\"ive}{101}{subsection.7.5.3}
\contentsline {subsection}{\numberline {7.5.4}Rabin-Karp}{103}{subsection.7.5.4}
\contentsline {section}{\numberline {7.6}Summary}{105}{section.7.6}
\contentsline {chapter}{\numberline {8}Further Algorithm Comparison}{107}{chapter.8}
\contentsline {section}{\numberline {8.1}Performance versus input length with no matches}{108}{section.8.1}
\contentsline {subsection}{\numberline {8.1.1}Horspool}{108}{subsection.8.1.1}
\contentsline {subsection}{\numberline {8.1.2}Quick Search}{109}{subsection.8.1.2}
\contentsline {subsection}{\numberline {8.1.3}Not So Na\"ive}{111}{subsection.8.1.3}
\contentsline {subsection}{\numberline {8.1.4}Rabin-Karp}{112}{subsection.8.1.4}
\contentsline {section}{\numberline {8.2}Performance versus number of matches}{114}{section.8.2}
\contentsline {subsection}{\numberline {8.2.1}Horspool}{114}{subsection.8.2.1}
\contentsline {subsection}{\numberline {8.2.2}Quick Search}{115}{subsection.8.2.2}
\contentsline {subsection}{\numberline {8.2.3}Not So Na\"ive}{115}{subsection.8.2.3}
\contentsline {subsection}{\numberline {8.2.4}Rabin-Karp}{116}{subsection.8.2.4}
\contentsline {section}{\numberline {8.3}How does multithreading affect processing speed?}{118}{section.8.3}
\contentsline {subsection}{\numberline {8.3.1}Horspool}{120}{subsection.8.3.1}
\contentsline {subsection}{\numberline {8.3.2}Quick Search}{121}{subsection.8.3.2}
\contentsline {subsection}{\numberline {8.3.3}Not So Na\"ive}{122}{subsection.8.3.3}
\contentsline {subsection}{\numberline {8.3.4}Rabin-Karp}{123}{subsection.8.3.4}
\contentsline {section}{\numberline {8.4}Summary}{124}{section.8.4}
\contentsline {chapter}{\numberline {9}Conclusion}{126}{chapter.9}
\contentsline {section}{\numberline {9.1}Document Recap}{126}{section.9.1}
\contentsline {section}{\numberline {9.2}Research Objectives}{127}{section.9.2}
\contentsline {section}{\numberline {9.3}Future Work}{128}{section.9.3}
\contentsline {chapter}{\numberline {10}References}{131}{chapter.10}
\contentsline {chapter}{Appendix \numberline {A}}{142}{Appendix.1.A}
\contentsline {chapter}{Appendix \numberline {B}}{144}{Appendix.1.B}
