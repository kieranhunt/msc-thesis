What follows are the notes regarding corrections from the examiners.

## Examiner 1

> I realised that I had missed a discussion of the implementation of the
algorithms.

I've made a note in Implementation chapter introduction noting that the implementation of each of the algorithms followed the designs outlined in their originating papers.

> [...] I take it that he has implemented the string algorithms himself. Perphaps he used a library. Either way, this needs to be clearly explained.

In the same paragraph as the above I've noted that I reimplemented the algorithms myself.

> Ideally, the code should be made available in some publicly accessible repository.

I realise that I've completely forgotten to do that. I've open sourced the GitHub repo and linked to it.

> "use of deep packet inspection has not been widely used". I disagree [...]

Removed that line.

> The motivation for choosing the two worst performing algorithms is very weak

I've elaborated in the start of Chaper 8 about how the algorithms which represent the edges of the speed spectrum help to exemplify the factors which contribute to deep packet inspection performance (be it good or bad).

> Why not real data from a network?

Dataset A is real-world data from a network. I speak about my decision to use artificially created network traffic in Chapter 4.

The initial algorithm comparison was made using Dataset A and so the algorithms were originally compared using real-world data before being pared off.

> The choice of rules needs some motivation.

I've added a bit more motivation where I presented the rules. Namely that the domain-name based rules might feature frequently in a corporate firewall.

> It is not argued why DNS packets are the most suitable protocols to use.

DNS packets are really only the starting point. The synthetic packets don't really resemble a 'valid' DNS packet.

> The candidatw has chosen to not compare summary statistics of all algorithms on a single graph.

I chose to not have them on the same graph as I was trying to emphasize the shape of the data. Having them all on the same graph would mean that that I would lose their shape - especially the faster ones. I also thought of doing a log graph - which wouldn't squish the faster ones to the bottom but would again hide their shapes.

> There are often dangers in showing graphs where `x = 0` and `y = 0` axes are not shown.

Again I've already compared the raw speed of the algorithms using the bar charts. Those charts have their time axes anchored at zero. In the graphs following those (I assume these are the ones the examiner is referencing) I'm trying to show the shape of the algorithms for varying values on the x-axis.

> The figures don't look good printed.

Well I'm glad the examiner liked the electronic versions. I spent a lot of time tweaking them. I ended up going with versions that look good electronic as I assumed the thesis probably would never be printed after handing in. Thoughts?

> You have used hyphens when you should've used dashes

Hmmm. I'm used to my text editor sorting that out for me. I've fixed it.

> Therefore vs therefor

Fixed.

> Chapter 6 is a little lopsided.

I've bolstered the summary a bit.


> Rabin-karp quadracity

Fixed.

> Parallelism - critical discussion and self-reflection

The examiner makes a good point about alternative parallelism implementations. I've added a few paragraphs to that section touching on them and noting that they were not investigated in this research.

> Section 7.4's variation on times. Given a range of network flow - average performance is the best. Deterministic processing speed is the wrong term.

I agree with the examiner here - amazing what a year of working in the industry can do 😆. I've updated my wording to use bounded instead of deterministic. I've also added a paragraph talking about percentile-based latencies which I believe are more prevalent metrics than upper bound (P100) and even more than average (which is often biased towards that P100).

> Big-O vs Big-Ө

I'm actually a little confused here. I seem to remember making the distinction in the paper when I wrote it but I can't seem to find it.

I've updated it.

> Conclusion

> -1 Typographical errors

TODO

> -2 Correction of Rabin-Karp claim

Done.

> -3 Brief discussion of approaches to parallelism

Done.

## Examiner 2

> The literature review is insufficient.

> Too many older publications - not enough newer ones.

I was going to ask how many more 'newer' publications would constitute a satisfactory level and what year constitutes 'new' but I feel like that's like asking 'how long is a piece of string?'.

I've added a couple of more-recent references. I've bolstered particularly the encryption section.

> a superficial search reveal some publications using string matching algorithms for DPI

I haven't found anything that seems to have done what I have - at least no one seems to have compared as many algorithms as I have.

I did find one interesting paper (Liao, 2015) which I've added a reference to.

> Summaries for each of the considered algorithms is insufficient.

I disagree here. I feel like I spent enough time going over each algorithm.

During the actual experimental comparison I go into further detail about the four chosen algorithms - thoughts?

> Section 6.1.1

Updated with reasoning.

> Information in 6.1.3 is not useful. Explain the statistical calculations that will be done.

I'm just explaining the output of the system at this point. The statistical calculations come later. I ended up using R and the raw data more than these statistical calculations anyway.

> Section 7.5.1

I've tried to make my points more clear by speaking to the annotations already on the graphs. I've also referenced the tables again there.

> Call out of deterministic again.

Fixed along with previous examiner's points

> Selection of algorithms is not convincing

I feel that I've made it clear that my selection of algorithms was based how interesting I found them. Namely that their performance was not as well known at the these smaller lengths of text.

> Big-O

I've since corrected this to Big-Ө.

> Why did you use a coefficient of 1000 in figure 7.4?

The point of those graphs was to show that - for smaller values of `n` - the behaviour of the algorithms can't just be summised by the algorithmic complexity.

> paragraph 7 is a mess

I've fixed it up.

> Dataset E has not been used in the experiments

The point of dataset E is to show how I went about going from dataset D to dataset F. I can remove it if you want but I feel like it provides insight into my reasoning.

> Section 8.1 - justify why this algorithm behaves the way it does

Added some justification there.

> Section 8.1.2 - justify

Added.

> 8.1.3 - justify

> Language and typographical errors

Fixed all.
