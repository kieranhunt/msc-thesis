\BOOKMARK [-1][-]{part.1}{I Introduction}{}% 1
\BOOKMARK [0][-]{chapter.1}{Introduction}{part.1}% 2
\BOOKMARK [1][-]{section.1.1}{Problem Statement}{chapter.1}% 3
\BOOKMARK [1][-]{section.1.2}{Research Outline}{chapter.1}% 4
\BOOKMARK [1][-]{section.1.3}{Research Method}{chapter.1}% 5
\BOOKMARK [1][-]{section.1.4}{Document Conventions}{chapter.1}% 6
\BOOKMARK [1][-]{section.1.5}{Document Structure}{chapter.1}% 7
\BOOKMARK [0][-]{chapter.2}{Literature Review}{part.1}% 8
\BOOKMARK [1][-]{section.2.1}{General Network Security}{chapter.2}% 9
\BOOKMARK [2][-]{subsection.2.1.1}{Network Security Tenets}{section.2.1}% 10
\BOOKMARK [2][-]{subsection.2.1.2}{Common Network Security Threats}{section.2.1}% 11
\BOOKMARK [2][-]{subsection.2.1.3}{Network Threat Mitigation Techniques}{section.2.1}% 12
\BOOKMARK [2][-]{subsection.2.1.4}{Summary}{section.2.1}% 13
\BOOKMARK [1][-]{section.2.2}{Firewalls}{chapter.2}% 14
\BOOKMARK [2][-]{subsection.2.2.1}{Layer 7 - Application Layer}{section.2.2}% 15
\BOOKMARK [2][-]{subsection.2.2.2}{Layer 4 - Transport Layer}{section.2.2}% 16
\BOOKMARK [2][-]{subsection.2.2.3}{Layer 3 - Network Layer}{section.2.2}% 17
\BOOKMARK [2][-]{subsection.2.2.4}{Network Address Translation}{section.2.2}% 18
\BOOKMARK [2][-]{subsection.2.2.5}{Other Firewalling Techniques}{section.2.2}% 19
\BOOKMARK [2][-]{subsection.2.2.6}{Denial of Service}{section.2.2}% 20
\BOOKMARK [2][-]{subsection.2.2.7}{Shortfalls}{section.2.2}% 21
\BOOKMARK [2][-]{subsection.2.2.8}{Summary}{section.2.2}% 22
\BOOKMARK [1][-]{section.2.3}{Intrusion Detection Systems}{chapter.2}% 23
\BOOKMARK [2][-]{subsection.2.3.1}{IDS Rules}{section.2.3}% 24
\BOOKMARK [2][-]{subsection.2.3.2}{Effectiveness}{section.2.3}% 25
\BOOKMARK [2][-]{subsection.2.3.3}{Performance}{section.2.3}% 26
\BOOKMARK [1][-]{section.2.4}{Packet Inspection}{chapter.2}% 27
\BOOKMARK [2][-]{subsection.2.4.1}{Shallow Packet Inspection}{section.2.4}% 28
\BOOKMARK [2][-]{subsection.2.4.2}{Medium Packet Inspection}{section.2.4}% 29
\BOOKMARK [2][-]{subsection.2.4.3}{Deep Packet Inspection}{section.2.4}% 30
\BOOKMARK [2][-]{subsection.2.4.4}{Encrypted Traffic}{section.2.4}% 31
\BOOKMARK [2][-]{subsection.2.4.5}{Why Perform DPI?}{section.2.4}% 32
\BOOKMARK [1][-]{section.2.5}{Summary}{chapter.2}% 33
\BOOKMARK [0][-]{chapter.3}{Algorithms}{part.1}% 34
\BOOKMARK [1][-]{section.3.1}{Stringology Primer}{chapter.3}% 35
\BOOKMARK [1][-]{section.3.2}{Na\357ve}{chapter.3}% 36
\BOOKMARK [1][-]{section.3.3}{Morris-Pratt}{chapter.3}% 37
\BOOKMARK [1][-]{section.3.4}{Knuth-Morris-Pratt}{chapter.3}% 38
\BOOKMARK [1][-]{section.3.5}{Boyer-Moore}{chapter.3}% 39
\BOOKMARK [1][-]{section.3.6}{Horspool}{chapter.3}% 40
\BOOKMARK [1][-]{section.3.7}{Rabin-Karp}{chapter.3}% 41
\BOOKMARK [1][-]{section.3.8}{Zhu-Takaoka}{chapter.3}% 42
\BOOKMARK [1][-]{section.3.9}{Quick Search}{chapter.3}% 43
\BOOKMARK [1][-]{section.3.10}{Smith}{chapter.3}% 44
\BOOKMARK [1][-]{section.3.11}{Apostolico-Crochemore}{chapter.3}% 45
\BOOKMARK [1][-]{section.3.12}{Colussi}{chapter.3}% 46
\BOOKMARK [1][-]{section.3.13}{Raita}{chapter.3}% 47
\BOOKMARK [1][-]{section.3.14}{Galil-Gaincarlo}{chapter.3}% 48
\BOOKMARK [1][-]{section.3.15}{Bitap}{chapter.3}% 49
\BOOKMARK [1][-]{section.3.16}{Simon}{chapter.3}% 50
\BOOKMARK [1][-]{section.3.17}{Not So Naive}{chapter.3}% 51
\BOOKMARK [1][-]{section.3.18}{Turbo Boyer-Moore}{chapter.3}% 52
\BOOKMARK [1][-]{section.3.19}{Reverse Colussi}{chapter.3}% 53
\BOOKMARK [1][-]{section.3.20}{Summary}{chapter.3}% 54
\BOOKMARK [0][-]{chapter.4}{Datasets}{part.1}% 55
\BOOKMARK [1][-]{section.4.1}{Dataset A}{chapter.4}% 56
\BOOKMARK [1][-]{section.4.2}{Dataset B}{chapter.4}% 57
\BOOKMARK [1][-]{section.4.3}{Dataset C}{chapter.4}% 58
\BOOKMARK [1][-]{section.4.4}{Dataset D}{chapter.4}% 59
\BOOKMARK [1][-]{section.4.5}{Dataset E}{chapter.4}% 60
\BOOKMARK [1][-]{section.4.6}{Dataset F}{chapter.4}% 61
\BOOKMARK [1][-]{section.4.7}{Summary}{chapter.4}% 62
\BOOKMARK [-1][-]{part.2}{II Packet Inspection Framework}{}% 63
\BOOKMARK [0][-]{chapter.5}{Design}{part.2}% 64
\BOOKMARK [1][-]{section.5.1}{Introduction}{chapter.5}% 65
\BOOKMARK [1][-]{section.5.2}{Overall Design}{chapter.5}% 66
\BOOKMARK [1][-]{section.5.3}{Input}{chapter.5}% 67
\BOOKMARK [1][-]{section.5.4}{Processing}{chapter.5}% 68
\BOOKMARK [1][-]{section.5.5}{Statistics Generation}{chapter.5}% 69
\BOOKMARK [1][-]{section.5.6}{Statistical Output}{chapter.5}% 70
\BOOKMARK [1][-]{section.5.7}{Raw Output}{chapter.5}% 71
\BOOKMARK [1][-]{section.5.8}{Summary}{chapter.5}% 72
\BOOKMARK [0][-]{chapter.6}{Implementation}{part.2}% 73
\BOOKMARK [1][-]{section.6.1}{Example Test}{chapter.6}% 74
\BOOKMARK [2][-]{subsection.6.1.1}{Program Startup}{section.6.1}% 75
\BOOKMARK [2][-]{subsection.6.1.2}{Testing}{section.6.1}% 76
\BOOKMARK [2][-]{subsection.6.1.3}{Statistics Generation}{section.6.1}% 77
\BOOKMARK [2][-]{subsection.6.1.4}{Output}{section.6.1}% 78
\BOOKMARK [2][-]{subsection.6.1.5}{Test Configuration}{section.6.1}% 79
\BOOKMARK [2][-]{subsection.6.1.6}{Statistics Output}{section.6.1}% 80
\BOOKMARK [2][-]{subsection.6.1.7}{Raw Results Output}{section.6.1}% 81
\BOOKMARK [1][-]{section.6.2}{Summary}{chapter.6}% 82
\BOOKMARK [-1][-]{part.3}{III Testing and Analysis}{}% 83
\BOOKMARK [0][-]{chapter.7}{Initial Algorithm Comparison}{part.3}% 84
\BOOKMARK [1][-]{section.7.1}{Rules}{chapter.7}% 85
\BOOKMARK [1][-]{section.7.2}{Test Hardware}{chapter.7}% 86
\BOOKMARK [1][-]{section.7.3}{Algorithm Performance?}{chapter.7}% 87
\BOOKMARK [2][-]{subsection.7.3.1}{[section-dataseta]Dataset a}{section.7.3}% 88
\BOOKMARK [2][-]{subsection.7.3.2}{[section-datasetb]Dataset b}{section.7.3}% 89
\BOOKMARK [1][-]{section.7.4}{Which algorithms vary the most?}{chapter.7}% 90
\BOOKMARK [1][-]{section.7.5}{Length Impact on Performance}{chapter.7}% 91
\BOOKMARK [2][-]{subsection.7.5.1}{Horspool}{section.7.5}% 92
\BOOKMARK [2][-]{subsection.7.5.2}{Quick Search}{section.7.5}% 93
\BOOKMARK [2][-]{subsection.7.5.3}{Not So Na\357ve}{section.7.5}% 94
\BOOKMARK [2][-]{subsection.7.5.4}{Rabin-Karp}{section.7.5}% 95
\BOOKMARK [1][-]{section.7.6}{Summary}{chapter.7}% 96
\BOOKMARK [0][-]{chapter.8}{Further Algorithm Comparison}{part.3}% 97
\BOOKMARK [1][-]{section.8.1}{Performance versus input length with no matches}{chapter.8}% 98
\BOOKMARK [2][-]{subsection.8.1.1}{Horspool}{section.8.1}% 99
\BOOKMARK [2][-]{subsection.8.1.2}{Quick Search}{section.8.1}% 100
\BOOKMARK [2][-]{subsection.8.1.3}{Not So Na\357ve}{section.8.1}% 101
\BOOKMARK [2][-]{subsection.8.1.4}{Rabin-Karp}{section.8.1}% 102
\BOOKMARK [1][-]{section.8.2}{Performance versus number of matches}{chapter.8}% 103
\BOOKMARK [2][-]{subsection.8.2.1}{Horspool}{section.8.2}% 104
\BOOKMARK [2][-]{subsection.8.2.2}{Quick Search}{section.8.2}% 105
\BOOKMARK [2][-]{subsection.8.2.3}{Not So Na\357ve}{section.8.2}% 106
\BOOKMARK [2][-]{subsection.8.2.4}{Rabin-Karp}{section.8.2}% 107
\BOOKMARK [1][-]{section.8.3}{How does multithreading affect processing speed?}{chapter.8}% 108
\BOOKMARK [2][-]{subsection.8.3.1}{Horspool}{section.8.3}% 109
\BOOKMARK [2][-]{subsection.8.3.2}{Quick Search}{section.8.3}% 110
\BOOKMARK [2][-]{subsection.8.3.3}{Not So Na\357ve}{section.8.3}% 111
\BOOKMARK [2][-]{subsection.8.3.4}{Rabin-Karp}{section.8.3}% 112
\BOOKMARK [1][-]{section.8.4}{Summary}{chapter.8}% 113
\BOOKMARK [0][-]{chapter.9}{Conclusion}{part.3}% 114
\BOOKMARK [1][-]{section.9.1}{Document Recap}{chapter.9}% 115
\BOOKMARK [1][-]{section.9.2}{Research Objectives}{chapter.9}% 116
\BOOKMARK [1][-]{section.9.3}{Future Work}{chapter.9}% 117
\BOOKMARK [0][-]{chapter.10}{References}{part.3}% 118
\BOOKMARK [0][-]{Appendix.1.A}{Appendix }{part.3}% 119
\BOOKMARK [0][-]{Appendix.1.B}{Appendix }{part.3}% 120
