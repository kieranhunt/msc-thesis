\contentsline {lstlisting}{\numberline {2.1}Snort rule structure}{25}{lstlisting.2.1}
\contentsline {lstlisting}{\numberline {2.2}A very simple working Snort rule}{26}{lstlisting.2.2}
\contentsline {lstlisting}{\numberline {2.3}Snort rule featuring a static pattern and a regular expression}{26}{lstlisting.2.3}
\contentsline {lstlisting}{\numberline {2.4}Example of 5-Tuple based configuration}{31}{lstlisting.2.4}
\contentsline {lstlisting}{\numberline {4.1}Creating 10000 random DNS packets for \hyperref [section-datasetc]{\textit {Dataset \uppercase {c}}}\xspace }{55}{lstlisting.4.1}
\contentsline {lstlisting}{\numberline {4.2}Creating a bare DNS packet with Python and Scapy}{58}{lstlisting.4.2}
\contentsline {lstlisting}{\numberline {6.1}Example test configuration JSON file.}{78}{lstlisting.6.1}
\contentsline {lstlisting}{\numberline {6.2}Running the test system.}{79}{lstlisting.6.2}
\contentsline {lstlisting}{\numberline {6.3}Example statistical output.}{80}{lstlisting.6.3}
\contentsline {lstlisting}{\numberline {6.4}Example raw results output}{82}{lstlisting.6.4}
\contentsline {lstlisting}{\numberline {B.1}Example code for editing and creating PCAP files with Python and Scapy}{144}{lstlisting.1.B.1}
